# CIDENET Employees Front

Este es el front de nuestra aplicación de administración y mantenimiento de empleados de CIDENET SAS.

Fué desarrollado con Angular io y conectada al backend con NodeJS y Bases de Datos Mongo.

---
## Requerimientos

### Versión de Producción

Esta aplicación cuenta con una versión corriendo en Heroku como un adicional y para que sea visible en un entorno de producción. El enlace en el cuál se encuentra publicado este proyecto es: [Heroku link de producción](https://cidenet-heroku-front.herokuapp.com/)

### Versión de Desarrollo

### Requerimientos

### Node: 

Instalar Node en su versión más estable. Acá tienes unas pequeñas instrucciones dependiendo de tu sistema operativo:

- #### Instalación en Windows

  Abre el [Sitio Web Oficial de Node](https://nodejs.org/) y descarga la versión estable del instalador. 

- #### Instalación en Ubuntu

  Para instalar Node en Ubuntu solo debes seguir y ejecutar los siguientes comandos:

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Otros Sistemas Operativos

  Para instalaciones en otros sistemas por favor sigue las instrucciones en las páginas oficiale de  [Node](https://nodejs.org/) y de [NPM](https://npmjs.org/).

Si la instalción se realizó correctamente puedes validar ejecutando el siguiente comando: (La versión puede varias dependiendo del equipo y el momento de la instalación).

    $ node --version
    v10.16.0

    $ npm --version
    6.9.2

###

---

## Instalación del Proyecto

    $ git clone https://gitlab.com/colsoftware/cidenet-front.git
    $ cd cidenet-front
    $ npm install

    Una vez hayas ejecutado estos comandos procedemos a configurar la conexión con nuestro backend.

## Configuración del Proyecto

En la ruta de nuestro proyecto debemos abrir el archivo config.service.ts que se encuentra en la ruta `src/app/services/config.service.ts` y realizar los siguientes ajustes:

- En la variable this.apiURL debes configurar la ruta exacta donde se esté ejecutando el back de la aplicación. La Url por defecto debería ser http://localhost:3000/api

## Correr el Proyecto
    Una ves instalado todos los requerimientos y realizadas las configuraciones podemos correr nuestro proyecto con el siguiente comando:

    $ ng serve -o

    Si todo salió bien tendrás el proyecto ejecutandose en la siguiente http://localhost:4200/
    
---

