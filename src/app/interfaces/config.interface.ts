export interface ValidatorInterface {
    error: boolean;
    message: string;
}