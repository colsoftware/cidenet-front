export interface User {
    _id: string,
    name: string,
    email: string,
    phone: string,
    role: string,
    state: boolean,
    __v: number
}

export interface LoginUser {
    ok: boolean,
    data: User,
    err: string,
    token: string
}

export interface RegisterUser {
    ok: boolean,
    data: User,
    err: string
}

export interface LoginCredential {
    email: string
    password: string
}

export interface RegisterCredential {
    name: string,
      email: string,
      phone: string,
      password: string
}