export interface Employee {
    _id: string,
    name1: string,
    name2: string,
    lastName1: string,
    lastName2: string,
    country: string,
    identificationType: string,
    identificationNumber: string,
    email: string,
    enterDate: Date,
    area: string
}

export interface RegisterEmployee {
    _id: string,
    name1: string,
    name2: string,
    lastName1: string,
    lastName2: string,
    country: string,
    identificationType: string,
    identificationNumber: string,
    email: string,
    enterDate: string,
    area: string,
    registerDate: string,
    editDate: string,
    state: boolean,
    __v: number
}

export interface EmployeeResponse {
    ok: boolean,
    count: number,
    total: number,
    err: string,
    data: Employee[]
}

export interface EmployeeRegisterResponse {
    ok: boolean,
    err: {
        message: string
    },
    data: RegisterEmployee
}