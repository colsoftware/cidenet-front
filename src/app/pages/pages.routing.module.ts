import { RouterModule, Routes } from '@angular/router';
import { EmployeesComponent } from '../components/employees/employees.component';
import { ListEmployeesComponent } from '../components/employees/list-employees/list-employees.component';
import { CreateEmployeesComponent } from '../components/employees/create-employees/create-employees.component';
import { UpdateEmployeesComponent } from '../components/employees/update-employees/update-employees.component';

const pagesRoutes: Routes = [
    {
        path: 'employees',
        component: EmployeesComponent,
        children: [
            { path: 'list', component: ListEmployeesComponent, data: { title: 'Listado de empleados' } },
            { path: 'create', component: CreateEmployeesComponent, data: { title: 'Nuevo empleado Producto' } },
            { path: 'update/:id', component: UpdateEmployeesComponent, data: { title: 'Modificar Producto' } },
            { path: '**', pathMatch: 'full', redirectTo: 'list' }
        ]
    },
    { path: '', redirectTo: '/employees', pathMatch: 'full' }
];

export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );