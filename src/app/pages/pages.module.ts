import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Imports
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { PAGES_ROUTES } from './pages.routing.module';

// Components
import { PagesComponent } from './pages.component';
import { EmployeesComponent } from '../components/employees/employees.component';
import { ListEmployeesComponent } from '../components/employees/list-employees/list-employees.component';
import { CreateEmployeesComponent } from '../components/employees/create-employees/create-employees.component';
import { UpdateEmployeesComponent } from '../components/employees/update-employees/update-employees.component';

@NgModule({
    declarations: [
      EmployeesComponent,
      ListEmployeesComponent,
      CreateEmployeesComponent,
      UpdateEmployeesComponent,
      PagesComponent
    ],
    exports: [
      PagesComponent,
      EmployeesComponent,
      ListEmployeesComponent,
      CreateEmployeesComponent,
      UpdateEmployeesComponent
    ],
    imports: [
      CommonModule,
      RouterModule,
      FormsModule,
      ReactiveFormsModule,
      SharedModule,
      PAGES_ROUTES
    ]
  })
  export class PagesModule { }