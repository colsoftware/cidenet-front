import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// Services
import { EmployeeService } from 'src/app/services/services.index';
import { LocalStorageService } from 'src/app/services/localStorage/local-storage.service';

// Interfaces
import { ValidatorInterface } from '../../../interfaces/config.interface';
import { EmployeeRegisterResponse } from 'src/app/interfaces/employee.interface';

// Imports
import Swal from 'sweetalert2';

// Declarations
declare var $: any;
@Component({
  selector: 'app-create-employees',
  templateUrl: './create-employees.component.html',
  styleUrls: ['./create-employees.component.css']
})
export class CreateEmployeesComponent implements OnInit {
  employeeForm: FormGroup;

  execute = false;

  constructor(
    private fb: FormBuilder,
    private employeeService: EmployeeService,
    private router: Router,
    private localStorageService: LocalStorageService
  ) { }

  ngOnInit(): void {
    this.createForm();
    let today = new Date();
    today.setMonth(today.getMonth() - 1);
    
    setTimeout(() => {
      $('#initD').bootstrapMaterialDatePicker({
        weekStart: 0, format: 'DD/MM/YYYY HH:mm', shortTime : false, minDate: today
      });
    }, 500);
  }

  createForm(): void {
    this.employeeForm = this.fb.group({
      name1: ['', [Validators.required, Validators.maxLength(20)]],
      name2: ['', [Validators.maxLength(50)]],
      lastName1: ['', [Validators.required, Validators.maxLength(20)]],
      lastName2: ['', [Validators.required, Validators.maxLength(20)]],
      identificationType: ['', [Validators.required]],
      identificationNumber: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9-]*$')]],
      country: ['', [Validators.required]],
      enterDate: [''],
      area: ['', [Validators.required]]
    });
  }

  saveChanges() {
    if ( this.employeeForm.invalid ) {
      return Object.values(this.employeeForm.controls).forEach( control => {
        if ( control instanceof FormGroup  ){
          Object.values( control.controls ).forEach((controFormGroup) => controFormGroup.markAsTouched());
        }else{
          control.markAsTouched();
        }
      });
    }

    this.execute = true;

    Swal.fire({
      icon: 'info',
      text: 'Validando información. Por favor espere.',
      allowOutsideClick: false
    });

    Swal.showLoading();

    // Set EnterDate
    let tempDate = $('#initD')[0].value;

    this.employeeForm.controls['enterDate'].setValue(this.formatDate(tempDate));

    this.employeeService.registerEmployee(this.employeeForm.value)
        .subscribe( (result: EmployeeRegisterResponse) => {
          this.execute = false;
          if (result.ok === true) {
            Swal.close();

            Swal.fire({
              icon: 'success',
              title: 'Registro exitoso.',
              text: 'El registro del empleado se realizó exitosamente.'
            });

            this.router.navigate(['/employees', 'list']);
          } else {

            console.log(result.err.message)
            Swal.fire({
              icon: 'error',
              title: 'Atención',
              text: result.err.message
            });
            return;
          }
        },
        error => {
          this.execute = false;

          if(error.ok === false) {
            if(error.error) {
              Swal.fire({
                icon: 'warning',
                title: 'Atención',
                text: error.error.err.message
              });
              return;
            }
          }

          Swal.fire({
            icon: 'error',
            title: 'Atención',
            text: 'El sistema no se encuentra disponible'
          });
        });
  }

  formatDate(tempDate: string) {
    let arrayDate = tempDate.split(' ');
    let onlyDate = arrayDate[0].split('/');

    return `${ onlyDate[2] }-${ onlyDate[1] }-${ onlyDate[0] } ${ arrayDate[1] }`;
  }

  /* Validations Area */

  get name1NoValid(): ValidatorInterface{
    let message = 'El primer nombre es requerido';
    if ( this.employeeForm.get('name1').value.length > 20) {
      message = 'El primer nombre no debe exceder los 20 caracteres.';
    }

    return {
      error: this.employeeForm.get('name1').invalid && this.employeeForm.get('name1').touched,
      message
    };
  }

  get name2NoValid(): ValidatorInterface{
    let message = '';
    if ( this.employeeForm.get('name2').value.length > 50) {
      message = 'El segundo nombre no debe exceder los 50 caracteres.';
    }

    return {
      error: this.employeeForm.get('name2').invalid && this.employeeForm.get('name2').touched,
      message
    };
  }

  get lastName1NoValid(): ValidatorInterface{
    let message = 'El primer apellido es requerido';
    if ( this.employeeForm.get('lastName1').errors?.maxLength) {
      message = 'El primer apellido no debe exceder los 20 caracteres.';
    }

    return {
      error: this.employeeForm.get('lastName1').invalid && this.employeeForm.get('lastName1').touched,
      message
    };
  }

  get lastName2NoValid(): ValidatorInterface{
    let message = 'El segundo apellido es requerido';
    if ( this.employeeForm.get('lastName2').errors?.maxLength) {
      message = 'El segundo apellido no debe exceder los 20 caracteres.';
    }

    return {
      error: this.employeeForm.get('lastName2').invalid && this.employeeForm.get('lastName2').touched,
      message
    };
  }

  get identificationTypeNoValid(): ValidatorInterface{
    let message = 'El tipo de documento es requerido.';

    return {
      error: this.employeeForm.get('identificationType').invalid && this.employeeForm.get('identificationType').touched,
      message
    };
  }

  get identificationNumberNoValid(): ValidatorInterface{
    let message = 'El número de documento es requerido';
    if ( this.employeeForm.get('identificationNumber').errors?.maxLength) {
      message = 'El número de documento no debe exceder los 20 caracteres.';
    }

    if ( this.employeeForm.get('identificationNumber').errors?.pattern) {
      message = 'El número de documento debe ser alfanumerico. Solo admite guiones, números y letras mayúsculas y minúsculas.';
    }

    return {
      error: this.employeeForm.get('identificationNumber').invalid && this.employeeForm.get('identificationNumber').touched,
      message
    };
  }

  get countryNoValid(): ValidatorInterface{
    let message = 'El país es requerido.';

    return {
      error: this.employeeForm.get('country').invalid && this.employeeForm.get('country').touched,
      message
    };
  }

  get areaNoValid(): ValidatorInterface{
    let message = 'El área de trabajo es requerido.';

    return {
      error: this.employeeForm.get('area').invalid && this.employeeForm.get('area').touched,
      message
    };
  }

}
