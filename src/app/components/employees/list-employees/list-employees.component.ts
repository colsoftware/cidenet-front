import { Component, OnInit } from '@angular/core';

// Services
import { EmployeeService } from 'src/app/services/services.index';

// Interfaces
import { Employee, EmployeeRegisterResponse, EmployeeResponse } from 'src/app/interfaces/employee.interface';

// Imports
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

// Declarations
declare var $: any;
@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {
  table: any;

  filter: boolean = true;

  employeeList: Employee [] = [];
  constructor(private employeeService: EmployeeService, private router:Router) { 
    this.loadEmployees();
  }

  ngOnInit(): void {
    $('[data-toggle="tooltip"]').tooltip();
  }

  loadEmployees(): any{
    this.employeeList = [];

    if (this.table !== undefined){
      this.table.destroy();
      this.table = undefined;
    }

    this.employeeService.getAllEmployees(this.filter)
      .subscribe((result: EmployeeResponse) => {
        if(result) {
          if(result.ok === true) {
            this.employeeList = result.data;
            if (this.employeeList && this.employeeList.length > 0){
              if (this.table === undefined){
                setTimeout(() => {
                  this.table = $('#employeesTable').DataTable({
                    language: {
                      url: 'http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
                    }
                  });
                }, 500);
              }
            }
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Atención',
              text: result.err
            });
          }
        }
      },
      error => {
        Swal.fire({
          icon: 'error',
          title: 'Atención',
          text: 'El sistema no se encuentra disponible'
        });
      });
  }

  filterByState(state: boolean) {
    this.filter = state;
    this.loadEmployees();
  }

  modifyEmployee(employee: Employee) {
    this.router.navigate(['/employees', 'update', employee._id])
  }

  deleteEmployee(employee: Employee) {
    Swal.fire({
      icon: 'warning',
      title: 'Eliminar empleado',
      text: '¿Esta seguro que desea eliminar éste empleado?',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      customClass: {
        confirmButton: 'btn btn-danger mr-2',
        cancelButton: 'btn btn-info'
      },
      buttonsStyling: false,
      allowOutsideClick: false,
      allowEscapeKey: false
    }).then((action) => {
      if (action.isConfirmed){
        Swal.fire({
          icon: 'info',
          text: 'Espere por favor...',
          allowOutsideClick: false
        });

        Swal.showLoading();

        this.employeeService.deleteEmployee(employee._id).subscribe((result: EmployeeRegisterResponse) => {
          if (result.ok === true) {
            
            Swal.fire({
              icon: 'success',
              title: 'Eliminación exitosa',
              text: 'El empleado se ha eliminado exitosamente.',
              confirmButtonText: 'Aceptar',
              customClass: {
                confirmButton: 'btn btn-success',
              },
              buttonsStyling: false,
              allowOutsideClick: false,
              allowEscapeKey: false
            }).then(() => {
              this.loadEmployees();
            });
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Atención',
              confirmButtonText: 'Entendido',
              customClass: {
                confirmButton: 'btn btn-success',
              },
              buttonsStyling: false,
              text: result.err.message
            });
          }
        },
        error => {

          if(error.ok === false) {
            if(error.error) {
              Swal.fire({
                icon: 'warning',
                title: 'Atención',
                text: error.error.err.message
              });
              return;
            }
          }

          Swal.fire({
            icon: 'error',
            title: 'Atención',
            text: 'El sistema no se encuentra disponible'
          });
        });
      }else if (action.isDismissed){
        Swal.close();
      }
    });
  }

}
