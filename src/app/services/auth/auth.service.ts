import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from '../services.index';
import { LocalStorageService } from 'src/app/services/localStorage/local-storage.service';
import { Router } from '@angular/router';
import { User, LoginCredential, RegisterCredential } from '../../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // Declare the Main Route for API
  mainUrl = '';

  constructor(
    private configService: ConfigService,
    private http: HttpClient,
    private localStorage: LocalStorageService,
    private router: Router
  ) {
    this.mainUrl = this.configService.getAPIUrl();
  }

  get httpOptions(): any {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
  }

  getAllUsers(): any {
    return this.http.get(this.mainUrl + '/users', this.httpOptions);
  }

  getUser(id: string): any {
    return this.http.get(this.mainUrl + '/users/' + id, this.httpOptions);
  }

  registerClient(data: RegisterCredential): any{
    const body = JSON.stringify({
      name: data.name,
      email: data.email,
      phone: data.phone,
      password: data.password
    });

    return this.http.post(this.mainUrl + '/users/create', body, this.httpOptions);
  }

  login(data: LoginCredential): any {
    const body = JSON.stringify({
      email: data.email,
      password: data.password
    });

    return this.http.post(this.mainUrl + '/users/login', body, this.httpOptions);
  }

  isAutenticated(): boolean {

    const user: User = this.localStorage.getParsedObject('cidenet-user');
    if(!user || user._id === '') {
      this.router.navigateByUrl('/login');
      return false;
    }

    return true;
  }
}
