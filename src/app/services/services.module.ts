import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Import the Services Index
import {
  ConfigService,
  AuthService,
  EmployeeService
} from './services.index';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ConfigService,
    AuthService,
    EmployeeService
  ]
})
export class ServicesModule { }
