import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  set(key: string, value: string): any {
    return localStorage.setItem(key, value);
  }

  get(key: string): any {
    return localStorage.getItem(key);
  }

  setObject(key: string, value: any): any {
    return localStorage.setItem(key, JSON.stringify(value));
  }

  getObject(key: string): any {
    return localStorage.getItem(key);
  }

  getParsedObject(key: string): any {
    return JSON.parse(localStorage.getItem(key));
  }

  remove(key: string): any {
    localStorage.removeItem(key);
  }
}

