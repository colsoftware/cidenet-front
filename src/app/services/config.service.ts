import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  apiURL = '';
  constructor() {
    this.apiURL = 'http://localhost:3000/api';
  }

  getAPIUrl(): string {
    return this.apiURL;
  }
}
