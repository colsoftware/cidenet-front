import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ConfigService } from '../config.service';
import { LocalStorageService } from '../localStorage/local-storage.service';
import { RegisterEmployee } from 'src/app/interfaces/employee.interface';
import { User } from 'src/app/interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  // Declare the Main Route for API
  mainUrl: string = '';
  tokenUser: string = '';
  userData: User;

  constructor(
    private configService: ConfigService,
    private http: HttpClient,
    private localStorage: LocalStorageService,
  ) {
    this.mainUrl = this.configService.getAPIUrl();
    this.tokenUser = this.localStorage.getParsedObject('cidenet-token');
    this.userData = this.localStorage.getParsedObject('cidenet-user');
  }

  getAllEmployees(filter: boolean): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': this.tokenUser
      })
    };

    let query = '/employees?state=' + filter;
    console.log(query);

    return this.http.get(this.mainUrl + query, httpOptions);
  }

  getEmployee(id: string): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': this.tokenUser
      })
    };

    return this.http.get(this.mainUrl + '/employees/' + id, httpOptions);
  }

  registerEmployee(data: RegisterEmployee): any{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': this.tokenUser
      })
    };

    const body = JSON.stringify({
      name1: data.name1,
      name2: data.name2,
      lastName1: data.lastName1,
      lastName2: data.lastName2,
      country: data.country,
      identificationType: data.identificationType,
      identificationNumber: data.identificationNumber,
      enterDate: data.enterDate,
      area: data.area,
      user: this.userData._id
    });

    return this.http.post(this.mainUrl + '/employees/create', body, httpOptions);
  }

  updateEmployee(data: RegisterEmployee): any{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': this.tokenUser
      })
    };

    const body = JSON.stringify({
      name1: data.name1,
      name2: data.name2,
      lastName1: data.lastName1,
      lastName2: data.lastName2,
      country: data.country,
      identificationType: data.identificationType,
      identificationNumber: data.identificationNumber,
      enterDate: data.enterDate,
      area: data.area
    });

    return this.http.put(this.mainUrl + '/employees/update/' + data._id, body, httpOptions);
  }

  deleteEmployee(id: string): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': this.tokenUser
      })
    };

    return this.http.delete(this.mainUrl + '/employees/delete/' + id, httpOptions);
  }
}
