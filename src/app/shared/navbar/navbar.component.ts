import { Component, OnInit } from '@angular/core';

// Imports
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  constructor(
    private router: Router
  ) {}

  ngOnInit(): void {
  }

  logOut() {
    Swal.fire({
      icon: 'warning',
      title: 'Salír de la plataforma',
      text: '¿Esta seguro que desea salír?',
      showCancelButton: true,
      confirmButtonText: 'Sí',
      cancelButtonText: 'No',
      customClass: {
        confirmButton: 'btn btn-danger mr-2',
        cancelButton: 'btn btn-info'
      },
      buttonsStyling: false,
      allowOutsideClick: false,
      allowEscapeKey: false
    }).then((action) => {
      if (action.isConfirmed){
        Swal.fire({
          icon: 'info',
          text: 'Espere por favor...',
          allowOutsideClick: false
        });

        Swal.showLoading();

        this.router.navigate(['logout']);
        Swal.close();
      }else if (action.isDismissed){
        Swal.close();
      }
    });
  }

}
