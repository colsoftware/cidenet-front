import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// Services
import { AuthService } from 'src/app/services/services.index';
import { LocalStorageService } from 'src/app/services/localStorage/local-storage.service';

// Interfaces
import { ValidatorInterface } from '../../interfaces/config.interface';
import { LoginUser } from 'src/app/interfaces/user.interface';

// Imports
import Swal from 'sweetalert2';

// Declarations
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  execute = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private localStorageService: LocalStorageService
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm(): void {
    this.loginForm = this.fb.group({
      email        : ['admin@cidenet.com.co', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      password     : ['', [Validators.required] ]
    });
  }  

  login() {
    if ( this.loginForm.invalid ) {
      return Object.values(this.loginForm.controls).forEach( control => {
        if ( control instanceof FormGroup  ){
          Object.values( control.controls ).forEach((controFormGroup) => controFormGroup.markAsTouched());
        }else{
          control.markAsTouched();
        }
      });
    }

    this.execute = true;

    Swal.fire({
      icon: 'info',
      text: 'Validando información. Por favor espere.',
      allowOutsideClick: false
    });

    Swal.showLoading();

    this.authService.login(this.loginForm.value)
        .subscribe( (result: LoginUser) => {
          this.execute = false;
          if (result.ok === true) {

            // Set User Data in Local Storage
            this.localStorageService.remove('cidenet-user');
            this.localStorageService.setObject('cidenet-user', result.data);
            
            this.localStorageService.remove('cidenet-token');
            this.localStorageService.setObject('cidenet-token', result.token);

            Swal.close();

            this.router.navigate(['/employees', 'list']);
          } else {

            Swal.fire({
              icon: 'error',
              title: 'Atención',
              text: result.err
            });

          }
        },
        error => {
          this.execute = false;
          Swal.fire({
            icon: 'error',
            title: 'Atención',
            text: 'El sistema no se encuentra disponible'
          });
        });
  }

  /* Validations Area */
  get emailNoValid(): ValidatorInterface{
    let message = 'El correo electrónico es requerido.';
    if ( this.loginForm.get('email').errors?.pattern) {
      message = 'Debe ingresar un correo electrónico válido.';
    }

    return {
      error: this.loginForm.get('email').invalid && this.loginForm.get('email').touched,
      message
    };
  }

  get passwordNoValid(): boolean{
    return this.loginForm.get('password').invalid && this.loginForm.get('password').touched;
  }
}

