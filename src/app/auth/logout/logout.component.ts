import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../../services/localStorage/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
    private localStorage: LocalStorageService,
    private router: Router
  ) {
    this.localStorage.remove('cidenet-user');
    this.localStorage.remove('cidenet-token');

    // Go to Login Page
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
  }

}
