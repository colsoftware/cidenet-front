import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// Services
import { AuthService } from 'src/app/services/services.index';

// Interfaces
import { ValidatorInterface } from '../../interfaces/config.interface';
import { RegisterUser } from 'src/app/interfaces/user.interface';

// Imports
import Swal from 'sweetalert2';

// Declarations
declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  execute = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.createForm();
    console.log(this.execute);
  }

  createForm(): void {
    this.registerForm = this.fb.group({
      name         : ['Administrador CIDENET SAS', [Validators.required, Validators.minLength(2)] ],
      email        : ['admin@cidenet.com.co', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      phone        : ['3123563403', [Validators.required, Validators.minLength(10)]],
      password    : ['', [Validators.required] ]
    });
  }

  regiterClient(): void {
    if ( this.registerForm.invalid ) {
      return Object.values(this.registerForm.controls).forEach( control => {
        if ( control instanceof FormGroup  ){
          Object.values( control.controls ).forEach((controFormGroup) => controFormGroup.markAsTouched() );
        }else{
          control.markAsTouched();
        }
      });
    }

    this.execute = true;
    Swal.fire({
      icon: 'info',
      text: 'Validando información. Por favor espere.',
      allowOutsideClick: false
    });

    Swal.showLoading();

    this.authService.registerClient(this.registerForm.value)
        .subscribe( (result: RegisterUser) => {
          this.execute = false;
          if (result.ok === true) {
            Swal.close();

            Swal.fire({
              icon: 'success',
              title: 'Registro exitoso.',
              text: 'El registro del usuario se realizó exitosamente.'
            });
            this.router.navigate(['/login']);

          } else {
            Swal.fire({
              icon: 'error',
              title: 'Atención',
              text: result.err
            });
          }
        },
        error => {
          this.execute = false;
          Swal.fire({
            icon: 'error',
            title: 'Atención',
            text: 'El sistema no se encuentra disponible'
          });
        });
  }

  /* Validations Area */
  get nameNoValid(): ValidatorInterface{
    let message = 'El nombre es requerido';
    if ( this.registerForm.get('name').errors?.minlength) {
      message = 'El nombre debe tener más de dos caracteres.';
    }

    return {
      error: this.registerForm.get('name').invalid && this.registerForm.get('name').touched,
      message
    };
  }

  get emailNoValid(): ValidatorInterface{
    let message = 'El correo electrónico es requerido.';
    if ( this.registerForm.get('email').errors?.pattern) {
      message = 'Debe ingresar un correo electrónico válido.';
    }

    return {
      error: this.registerForm.get('email').invalid && this.registerForm.get('email').touched,
      message
    };
  }

  get phoneNoValid(): ValidatorInterface{
    let message = 'Teléfono es requerido.';
    if ( this.registerForm.get('phone').errors?.minlength) {
      message = `Teléfono debe contener más de ${this.registerForm.get('phone').errors?.minlength.requiredLength} caracteres.`;
    }

    return {
      error: this.registerForm.get('phone').invalid && this.registerForm.get('phone').touched,
      message
    };
  }

  get passwordNoValid(): boolean{
    return this.registerForm.get('password').invalid && this.registerForm.get('password').touched;
  }
}
